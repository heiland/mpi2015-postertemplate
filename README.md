LaTeX class following the MPI 2015 corporate design rules for
poster creation.

See the example directory for a sample poster explaining the usage.

Make sure you put the folder in a path that is recursively searched by latex. 
This can e.g. be your texmf folder in the home directory on a recent Ubuntu system.
Otherwise the logos will not be found.

The included comfort for TikZ users seems to require tikZ/PGF 3.00 or higher for 
proper nesting of elements without exceeding the TeX Capacities. TeXlive 2014 
has been reported to be sufficient to get the example.tex compiled.

The background color in the (content) node is slightly off of the value given in the Handbook. This is due to the fact, that it was wrong in all powerpoint templates and thus it was easier to fix it centrally by changing the color to the wrong value here in order to make the posters look alike.
