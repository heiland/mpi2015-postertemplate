import numpy as np

# base values from a0 paper -- do not alter!
a0size = [50   ,
28   ,
12   ,
14.4 ,
19   ,
22   ,
24   ,
32   ,
40   ,
43   ,
51.6 ,
62   ,
72   ,
89.16,
107  ,
100  ,
36   ,
54   ,
36   ,
24]

a0size1 = [
54    ,
32    ,
14    ,
16.8  ,
22.167,
25.667,
28    ,
37.333,
46.667,
50.167,
60.7  ,
72.333,
90    ,
112   ,
134   ,
106   ,
38    ,
60    ,
42    ,
28]

a3fac = 1./np.sqrt(2)**3  # This is the "right" scale
a3faclrg = 1./np.sqrt(2)**2.2
# This is the scale for increased font size

scalefac = a3faclrg

ftw = open('convscripsizes', 'w')

ftw.write('fontsizes\n')
for cursize in a0size:
    ftw.write('{0:.3f}\n'.format(scalefac*cursize))

ftw.write('skipsizes\n')
for cursize in a0size1:
    ftw.write('{0:.3f}\n'.format(scalefac*cursize))

ftw.close()
